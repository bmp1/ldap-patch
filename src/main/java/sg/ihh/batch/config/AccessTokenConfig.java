package sg.ihh.batch.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import sg.ihh.batch.manager.BaseManager;
import sg.ihh.batch.manager.EncryptionManager;
import sg.ihh.batch.model.AccessTokenResponse;
import sg.ihh.batch.util.http.HTTPClient;
import sg.ihh.batch.util.http.model.HTTPContentType;
import sg.ihh.batch.util.http.model.HTTPParameter;
import sg.ihh.batch.util.http.model.HTTPRequest;
import sg.ihh.batch.util.http.model.HTTPResponse;
import sg.ihh.batch.util.json.JsonUtil;
import sg.ihh.batch.util.log.AppLogger;
import sg.ihh.batch.util.property.OauthPropery;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

public class AccessTokenConfig extends BaseManager {

    private static AccessTokenConfig instance;

    private String accessToken;
    private LocalDateTime expiryDt;

    private  OauthPropery oauthPropery;

    public AccessTokenConfig(OauthPropery oauthPropery) {
        log = new AppLogger(this.getClass());
        this.oauthPropery = oauthPropery;
        updateToken();
    }

    public String getAccessToken() {
        if (expiryDt.isBefore(LocalDateTime.now())) {
            updateToken();
        }
        return accessToken;
    }
    @PostConstruct
    private void updateToken() {
        accessToken = getClientToken();
        expiryDt = LocalDateTime.now().plusMinutes(30);
    }

    private String getClientToken() {
        final String methodName = "getClientToken";
        //start(methodName);
        HTTPRequest request =
                new HTTPRequest.Builder(oauthPropery.getBaseUrl() + oauthPropery.getTokenUrl())
                        .setContentType(HTTPContentType.APPLICATION_FORM_URLENCODED).addHeader("Host", oauthPropery.getHostname()).build();

        HTTPParameter params = new HTTPParameter();
        params.addParameter("client_id", oauthPropery.getClientId());
        params.addParameter("client_secret", EncryptionManager.getInstance().decrypt(oauthPropery.getClientSecret()));
        params.addParameter("scope", oauthPropery.getClientScopes());
        params.addParameter("grant_type", "client_credentials");

        HTTPResponse httpResponse = HTTPClient.post(request, params);

        AccessTokenResponse response = JsonUtil.fromJson(httpResponse.getBody(), AccessTokenResponse.class);

        //completed(methodName);
        return response.getAccessToken();
    }
     public static AccessTokenConfig getInstance(OauthPropery oauthPropery) {
        if (instance == null) {
            instance = new AccessTokenConfig(oauthPropery);
        }
        return instance;
    }
}

