package sg.ihh.batch.config;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import sg.ihh.batch.util.helper.PropertyHelper;
import sg.ihh.batch.util.log.AppLogger;

public class BaseConfig {
    protected StringBuilder errorMsg;
    protected int lineCount;

    @Autowired
    protected Environment env;

    protected AppLogger log;

    protected AppLogger getLogger(Class<?> clazz) {
        return new AppLogger(clazz);
    }

    protected void start(String id, String methodName) {
        log.debug(id, methodName, "Start");
    }

    protected void completed(String id, String methodName) {
        log.debug(id, methodName, "Completed");
    }

    protected void start(String methodName) {
        log.debug(methodName, "Start");
    }

    protected void completed(String methodName) {
        log.debug(methodName, "Completed");
    }

    protected String getProp(String key) {
        return PropertyHelper.getProperty(key);
    }

    protected void putObjectExecutionContextValue(StepExecution stepExecution, String key, Object value) {
        stepExecution.getJobExecution().getExecutionContext().put(key, value);
    }

    protected void putExecutionContextValue(StepExecution stepExecution, String key, String value) {
        stepExecution.getJobExecution().getExecutionContext().putString(key, value);
    }

    protected void putDoubleExecutionContextValue(StepExecution stepExecution, String key, Double value) {
        stepExecution.getJobExecution().getExecutionContext().putDouble(key, value);
    }

    protected void putIntExecutionContextValue(StepExecution stepExecution, String key, int value) {
        stepExecution.getJobExecution().getExecutionContext().putInt(key, value);
    }

    protected String getExecutionContextValue(StepExecution stepExecution, String key) {
        if (stepExecution.getJobExecution().getExecutionContext().containsKey(key)) {
            return stepExecution.getJobExecution().getExecutionContext().getString(key);
        } else {
            return null;
        }
    }

    protected int getIntExecutionContextValue(ExecutionContext stepExecution, String key) {
        if (stepExecution.containsKey(key)) {
            return stepExecution.getInt(key);
        } else {
            return 0;
        }
    }

    protected double getDoubleExecutionContextValue(StepExecution stepExecution, String key) {
        if (stepExecution.getJobExecution().getExecutionContext().containsKey(key)) {
            return stepExecution.getJobExecution().getExecutionContext().getDouble(key);
        } else {
            return 0;
        }
    }
}
