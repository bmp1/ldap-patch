package sg.ihh.batch.util.helper;

import org.springframework.util.Base64Utils;
import sg.ihh.batch.util.log.AppLogger;

import java.nio.charset.StandardCharsets;

public class StringHelper {
    private static final AppLogger log = new AppLogger(StringHelper.class);

    private StringHelper() {
        // Empty Constructor
    }

    public static boolean validate(String str) {
        return str != null && !str.trim().isEmpty();
    }

    public static String base64Encode(String str) {
        return new String(Base64Utils.encode(str.getBytes()), StandardCharsets.UTF_8);
    }

    public static String base64Decode(String str) {
        return new String(Base64Utils.decode(str.getBytes(StandardCharsets.UTF_8)));
    }

    public static String toUpperCase(String str) {
        return str.toUpperCase();
    }
}
