package sg.ihh.batch.util.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import sg.ihh.batch.util.log.AppLogger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@PropertySource("classpath:application.properties")
@ConfigurationProperties
public class PropertyHelper implements EnvironmentAware {

    @Autowired
    private Environment env;

    private static AppLogger log = new AppLogger(PropertyHelper.class);

    private static PropertyHelper instance;

    public PropertyHelper() {
        // Empty Constructor
    }

    @Override
    public void setEnvironment(Environment environment) {
        getInstance().env = environment;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    public static String getProperty(String key) {
        return getProperty(key, "");
    }

    public static int getIntProperty(String key) {
        return getInstance().env.getProperty(key, Integer.class);
    }

    public static int getIntProperty(String key, int defaultValue) {
        return getInstance().env.getProperty(key, Integer.class, defaultValue);
    }

    public static long getLongProperty(String key) {
        return Long.parseLong(getProperty(key));
    }

    public static long getLongProperty(String key, long defaultValue) {
        return Long.parseLong(getProperty(key, String.valueOf(defaultValue)));
    }

    public static double getDoubleProperty(String key) {
        return Double.parseDouble(getProperty(key));
    }

    public static double getDoubleProperty(String key, double defaultValue) {
        return Double.parseDouble(getProperty(key, String.valueOf(defaultValue)));
    }

    public static boolean getBooleanProperty(String key) {
        return Boolean.parseBoolean(getProperty(key));
    }

    public static Date getDateProperty(String key, String format) {
        DateFormat df = new SimpleDateFormat(format);
        Date retDate = new Date();
        try {
            retDate = df.parse(getProperty(key));
        } catch (ParseException e) {
            retDate = new Date(System.currentTimeMillis());
        }
        return retDate;

    }

    private static String getProperty(String key, String defaultValue) {
        String methodName = "key";
        try {
            return getInstance().env.getRequiredProperty(key);

        } catch (Exception e) {
            log.error(methodName, e.getStackTrace());
        }
        return defaultValue;
    }

    public static PropertyHelper getInstance() {
        if (instance == null) {
            instance = new PropertyHelper();
        }
        return instance;
    }
}
