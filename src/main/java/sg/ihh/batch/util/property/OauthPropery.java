package sg.ihh.batch.util.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix="oauth")
@Validated
public class OauthPropery extends BaseProperty {

    private String hostname;
    private String baseUrl;
    private String clientId;
    private String clientSecret;
    private String clientScopes;
    private String tokenUrl;
    private String scimUrl;

    public OauthPropery() {
        log = getLogger(OauthPropery.class);
        log.info("OauthPropery Initiated");
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientScopes() {
        return clientScopes;
    }

    public void setClientScopes(String clientScopes) {
        this.clientScopes = clientScopes;
    }

    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
    }

    public String getScimUrl() {
        return scimUrl;
    }

    public void setScimUrl(String scimUrl) {
        this.scimUrl = scimUrl;
    }
}
