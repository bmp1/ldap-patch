package sg.ihh.batch.util.property;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@Validated
public class EncryptionProperty  extends BaseProperty {

    @Value("${app.server-key}")
    private String appServerKey;
    @Value("${api.gw.public-key}")
    private String apiGwPublicKey;

    public EncryptionProperty() {
        log = getLogger(EncryptionProperty.class);
        log.info("EncryptionProperty Initiated");
    }

    public String getAppServerKey() {
        return appServerKey;
    }

    public void setAppServerKey(String appServerKey) {
        this.appServerKey = appServerKey;
    }

    public String getApiGwPublicKey() {
        return apiGwPublicKey;
    }

    public void setApiGwPublicKey(String apiGwPublicKey) {
        this.apiGwPublicKey = apiGwPublicKey;
    }
}
