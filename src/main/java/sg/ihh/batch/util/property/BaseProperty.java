package sg.ihh.batch.util.property;


import sg.ihh.batch.util.log.AppLogger;

public class BaseProperty {
    protected AppLogger log;

    protected AppLogger getLogger(Class<?> clazz) {
        return new AppLogger(clazz);
    }

}
