package sg.ihh.batch.util.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix="ldap")
@Validated
public class LdapProperty extends BaseProperty {

    private String host;
    private int port;
    private String bindDn;
    private String bindPassword;
    private String baseDn;
    private int connectionsMin;
    private int connectionsMax;

    public LdapProperty() {
        log = getLogger(LdapProperty.class);
        log.info("LdapProperty Initiated");
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getBindDn() {
        return bindDn;
    }

    public void setBindDn(String bindDn) {
        this.bindDn = bindDn;
    }

    public String getBindPassword() {
        return bindPassword;
    }

    public void setBindPassword(String bindPassword) {
        this.bindPassword = bindPassword;
    }

    public String getBaseDn() {
        return baseDn;
    }

    public void setBaseDn(String baseDn) {
        this.baseDn = baseDn;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getConnectionsMin() {
        return connectionsMin;
    }

    public void setConnectionsMin(int connectionsMin) {
        this.connectionsMin = connectionsMin;
    }

    public int getConnectionsMax() {
        return connectionsMax;
    }

    public void setConnectionsMax(int connectionsMax) {
        this.connectionsMax = connectionsMax;
    }
}
