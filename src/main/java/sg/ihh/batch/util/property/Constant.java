package sg.ihh.batch.util.property;

public class Constant {

    private Constant() {}

    public static final String COMMON_PROPERTY_FILENAME         = "application.common.properties";

    public static final String PROPERTY_FILENAME                = "application.properties";

    // LDAP Default
    public static final int    DEFAULT_LDAP_CONNECTION_MIN      = 5;
    public static final int    DEFAULT_LDAP_CONNECTION_MAX      = 30;
    public static final String LDAP_SEARCH_ATTR_DOCTOR          = "pplMCR";
    public static final String LDAP_SEARCH_ATTR_CLINIC_ASSISTANT= "pplCALogin";
    public static final String LDAP_SEARCH_ATTR_PATIENT         = "mail";
    public static final String LDAP_SEARCH_ATTR_PATIENT_NRIC    = "pplID";
    public static final String LDAP_SDH_STATUS_TRUE             = "TRUE";
    public static final String LDAP_SDH_STATUS_FALSE            = "FALSE";
    public static final String LDAP_SEARCH_ATTR_MAIL            = "mail";

    // LDAP Error Message
    public static final String LDAP_EMAIL_ERROR = "Email already exists";

    // GLUU SCIM Schema
    public static final String SCIM_USER_CORE_SCHEMA      = "urn:ietf:params:scim:schemas:core:2.0:User";
    public static final String SCIM_USER_EXTENSION_SCHEMA = "urn:ietf:params:scim:schemas:extension:gluu:2.0:User";

}
