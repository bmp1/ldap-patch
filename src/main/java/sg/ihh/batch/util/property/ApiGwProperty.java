package sg.ihh.batch.util.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix="api.gw")
@Validated
public class ApiGwProperty extends BaseProperty {

    private String baseUrl;
    public ApiGwProperty() {
        log = getLogger(ApiGwProperty.class);
        log.info("ApiGwProperty Initiated");
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
