package sg.ihh.batch.util.constant;

public class Constants {

    private Constants() {
        // Empty Constructor
    }

    // Status
    public static final String START = "Start";
    public static final String END = "Completed";

    public static final String EXCEPTION = "Exception : ";
    public static final String PARAM_JOB_ID = "JobID";

    public static final String PARAM_MCR_SKIP_REASON = "MCR Number ({mcrNo}) not found in DB table";

    public static final String PARAM_ASSOC_TREATMENT_SKIP_REASON = "Associated treatment ({assocTreatment}) not found / duplicated in DB table";

}
