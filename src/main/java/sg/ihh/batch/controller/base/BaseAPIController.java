package sg.ihh.batch.controller.base;

import parkway.sdh.gw.encryption.model.Payload;
import sg.ihh.batch.config.AccessTokenConfig;
import sg.ihh.batch.manager.APIEncryptionManager;
import sg.ihh.batch.util.http.model.HTTPContentType;
import sg.ihh.batch.util.http.model.HTTPRequest;
import sg.ihh.batch.util.http.model.HTTPResponse;
import sg.ihh.batch.util.json.JsonUtil;
import sg.ihh.batch.util.property.ApiGwProperty;
import sg.ihh.batch.util.property.ApiUserProperty;
import sg.ihh.batch.util.property.OauthPropery;

import javax.ws.rs.core.Response;

public class BaseAPIController extends BaseController {

    private static final String BEARER = "Bearer ";

    private OauthPropery oauthPropery;
    private ApiUserProperty apiUserProperty;
    private ApiGwProperty apiGwProperty;


    public BaseAPIController(OauthPropery oauthPropery, ApiUserProperty apiUserProperty, ApiGwProperty apiGwProperty) {
        // Empty Constructor
        this.oauthPropery = oauthPropery;
        this.apiUserProperty = apiUserProperty;
        this.apiGwProperty = apiGwProperty;
    }

    // Hit Gluu Server
    protected HTTPRequest buildProtectedGluuAPIRequest(String url) {
        return new HTTPRequest.Builder(oauthPropery.getBaseUrl() + url)
                .setAuthorization(BEARER + AccessTokenConfig.getInstance(oauthPropery).getAccessToken())
                .setContentType(HTTPContentType.APPLICATION_JSON).addHeader("Host", oauthPropery.getHostname()).build();
    }

    // Hit API Gateway
    protected HTTPRequest buildProtectedAPIRequest(String url) {
        return new HTTPRequest.Builder(apiGwProperty.getBaseUrl() + url)
                .setContentType(HTTPContentType.APPLICATION_JSON).setAuthorization(BEARER + getAccessToken())
                .build();
    }

    protected HTTPRequest buildAPIRequest(String base, String url) {
        return new HTTPRequest.Builder(base + url).setContentType(HTTPContentType.APPLICATION_JSON)
                .setAuthorization(BEARER + getAccessToken()).build();
    }

    private String getAccessToken() {
        return AccessTokenConfig.getInstance(oauthPropery).getAccessToken();
    }

    protected <T> T parseResponse(HTTPResponse response, Class<T> clazz) {
        return JsonUtil.fromJson(response.getBody(), clazz);
    }

    protected void filteredLogRequest(String methodName, Object request) {
        if (apiUserProperty.isDoLog()) {
            log.debug(methodName, "Request: " + toJson(request));
        }
    }

    protected void filteredLogRequest(String methodName, HTTPRequest request, Object obj) {
        if (apiUserProperty.isDoLog()) {
            log.debug(methodName, "Request URL : " + request.getUrl());
            log.debug(methodName, "Request Body : " + toJson(obj));
        }
    }

    protected void logRequest(String methodName, HTTPRequest request, Object obj) {
        log.debug(methodName, "Request URL : " + request.getUrl());
        log.debug(methodName, "Request Body : " + toJson(obj));
    }

    protected void logRequest(String methodName, Object request) {
        log.debug(methodName, "Request: " + toJson(request));
    }

    protected void logResponse(String methodName, Object response) {
        log.debug(methodName, "Response :" + toJson(response));
    }

    protected void logResponse(String methodName, HTTPResponse response) {
        log.debug(methodName, "Response :" + response.getCode() + " : " + toJson(response.getBody()));
    }

    protected void filteredLogResponse(String methodName, Object response) {
        if (apiUserProperty.isDoLog()) {
            log.debug(methodName, "Response :" + toJson(response));
        }
    }

    protected void filteredLogResponse(String methodName, HTTPResponse response) {
        if (apiUserProperty.isDoLog()) {
            log.debug(methodName, "Response :" + response.getCode() + " : " + toJson(response.getBody()));
        }
    }


    protected String toJson(Object obj) {
        return JsonUtil.toJson(obj);
    }

    protected <T> T fromJson(String str, Class<T> clazz) {
        return JsonUtil.fromJson(str, clazz);
    }

    protected <T> T fromResponse(HTTPResponse response, Class<T> clazz) {
        return JsonUtil.fromJson(response.getBody(), clazz);
    }

    protected boolean isSuccessResponse(HTTPResponse response) {
        return isStatus(response, Response.Status.OK);
    }

    protected boolean isStatus(HTTPResponse response, Response.Status status) {
        return response.getCode() == status.getStatusCode();
    }

    protected String encryptRequest(Object request) {
        APIEncryptionManager apiEncryptionManager = APIEncryptionManager.getInstance();
        return toJson(apiEncryptionManager.encrypt(toJson(request)));
    }

    protected <T> T decryptResponse(HTTPResponse response, Class<T> clazz) {
        APIEncryptionManager apiEncryptionManager = APIEncryptionManager.getInstance();
        if (response.getCode() == Response.Status.OK.getStatusCode()) {
            Payload payload = fromResponse(response, Payload.class);
            return JsonUtil.fromJson(apiEncryptionManager.decrypt(payload), clazz);
        } else {

            return fromResponse(response, clazz);
        }
    }


    protected boolean checkStatus(HTTPResponse httpResponse, Response.Status status) {
        return httpResponse.getCode() == status.getStatusCode();
    }
}
