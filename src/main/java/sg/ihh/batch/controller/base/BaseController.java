package sg.ihh.batch.controller.base;


import sg.ihh.batch.util.http.model.HTTPContentType;
import sg.ihh.batch.util.http.model.HTTPRequest;
import sg.ihh.batch.util.log.AppLogger;

public class BaseController {

    protected AppLogger log;

    public BaseController() {
        // Empty Constructor
    }


    protected HTTPRequest buildAuthenticatedRequest(String path, String token) {
        return new HTTPRequest.Builder(path).addHeader("Authorization", "Bearer " + token)
                .setContentType(HTTPContentType.APPLICATION_JSON).build();
    }

    protected <T> AppLogger getLogger(Class<T> clazz) {
        return new AppLogger(clazz);
    }

    protected void start(String methodName) {
        log.debug(methodName, "Start");
    }

    protected void completed(String methodName) {
        log.debug(methodName, "Completed");
    }

}