package sg.ihh.batch.controller;

import org.springframework.stereotype.Controller;
import sg.ihh.batch.controller.base.BaseAPIController;
import sg.ihh.batch.model.User;
import sg.ihh.batch.model.UserRequest;
import sg.ihh.batch.model.scim.SCIMExtension;
import sg.ihh.batch.model.scim.SCIMSearchResponse;
import sg.ihh.batch.model.scim.SCIMUser;
import sg.ihh.batch.util.http.HTTPClient;
import sg.ihh.batch.util.http.model.HTTPParameter;
import sg.ihh.batch.util.http.model.HTTPRequest;
import sg.ihh.batch.util.http.model.HTTPResponse;
import sg.ihh.batch.util.property.ApiGwProperty;
import sg.ihh.batch.util.property.ApiUserProperty;
import sg.ihh.batch.util.property.Constant;
import sg.ihh.batch.util.property.OauthPropery;

import javax.ws.rs.core.Response.Status;
import java.util.Arrays;

@Controller
public class SCIMController extends BaseAPIController {

    private OauthPropery oauthPropery;
    private ApiGwProperty apiGwProperty;
    private ApiUserProperty apiUserProperty;

    public SCIMController(OauthPropery oauthPropery,ApiUserProperty apiUserProperty, ApiGwProperty apiGwProperty) {
        super(oauthPropery, apiUserProperty, apiGwProperty);
        log = getLogger(this.getClass());
        this.oauthPropery = oauthPropery;
        this.apiUserProperty = apiUserProperty;
        this.apiGwProperty = apiGwProperty;
    }

    public boolean update(User user, UserRequest request) {
        final String methodName = "update";
        boolean result = false;
        //start(methodName);

        // in order to update user via SCIM, we need INUM from Gluu
        // we need to pass in UID (which is userName in Gluu) in exchange for id (INUM in Gluu)
        SCIMSearchResponse sr = search(request);

        if (sr != null) {
            int resultCnt = sr.getTotalResults();   // expecting only 1 row

            if (resultCnt == 1) {
                String id = sr.getResourceList().get(0).getId();
                //log.debug(methodName, "INUM: " + id);

                HTTPRequest httpRequest = buildProtectedGluuAPIRequest(oauthPropery.getScimUrl() + "/" + id);

                // Prepare Request
                SCIMUser scimUser = generateUpdateUser(user, request);

                filteredLogRequest(methodName, httpRequest, scimUser);

                // Send Request
                HTTPResponse httpResponse = HTTPClient.put(httpRequest, toJson(scimUser), apiUserProperty.isDoLog());

                filteredLogResponse(methodName, httpResponse);

                result = checkStatus(httpResponse, Status.OK);

            } else {
                // log such occurrence, but still return true so that UI will not bomb
                log.info(methodName, "User record (" + request.getUid() + ") does not exist in GLUU.");
                result = false;
            }
        }

        //completed(methodName);
        return result;
    }

    private SCIMSearchResponse search(UserRequest request) {
        final String methodName = "search";
        //start(methodName);

        HTTPRequest httpRequest = buildProtectedGluuAPIRequest(oauthPropery.getScimUrl());

        // Prepare Request
        HTTPParameter params = new HTTPParameter();
        params.addParameter("filter", "userName eq \"" + request.getUid() + "\"");

        // Send Request
        filteredLogRequest(methodName, httpRequest, params);

        HTTPResponse httpResponse = HTTPClient.get(httpRequest, params, apiUserProperty.isDoLog());

        filteredLogResponse(methodName, httpResponse);

        SCIMSearchResponse response = fromJson(httpResponse.getBody(), SCIMSearchResponse.class);

        //completed(methodName);
        return response;
    }

    private SCIMUser generateUpdateUser(User user, UserRequest request) {
        final String methodName = "generateUpdateUser";
        //start(methodName);

        String sdhStatus = user.getSdhStatus();

        //
        // Patients - 1. Not allowed to change email address
        //            2. If sdhStatus == TRUE, not allowed to change DOB and NRIC
        //
        // Note: user is from LDAP; request is from FE
        //       those field(s) that is not allowed to change, take from user

        SCIMExtension extension = new SCIMExtension();
        extension.setGender           (request.getGender());
        extension.setMobileNumber     (request.getContactNo());
        extension.setMobileCountryCode(request.getCountryCode());
        extension.setEmployeeType     (request.getEmployeeType());

        if (Constant.LDAP_SDH_STATUS_FALSE.equals(sdhStatus)) {
            extension.setDob (request.getDob());
            extension.setNric(request.getNric());
        } else {
            extension.setDob (user.getDob());
            extension.setNric(user.getNric());
        }

        SCIMUser scimUser = new SCIMUser();
        scimUser.setSchemaList(Arrays.asList(Constant.SCIM_USER_CORE_SCHEMA, Constant.SCIM_USER_EXTENSION_SCHEMA));
        scimUser.setExtension(extension);
        scimUser.setActive(true);

        //completed(methodName);
        return scimUser;
    }

}
