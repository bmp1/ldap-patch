package sg.ihh.batch.controller;

import com.unboundid.ldap.sdk.*;

import sg.ihh.batch.controller.base.BaseController;
import sg.ihh.batch.config.LDAPConnectionConfig;
import sg.ihh.batch.model.LDAPAttribute;
import sg.ihh.batch.model.User;
import sg.ihh.batch.model.UserRequest;
import sg.ihh.batch.model.UserStatus;
import sg.ihh.batch.util.ldap.LDAPHelper;
import sg.ihh.batch.util.property.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LdapController extends BaseController {

    private LDAPConnectionConfig ldapConnectionConfig;
    private LdapProperty ldapProperty;
    private ApiUserProperty apiUserProperty;
    private OauthPropery oauthPropery;
    private ApiGwProperty apiGwProperty;

    public LdapController(LdapProperty ldapProperty,LDAPConnectionConfig ldapConnectionConfig,
                          ApiUserProperty apiUserProperty, OauthPropery oauthPropery, ApiGwProperty apiGwProperty) {
        log = getLogger(this.getClass());
        this.ldapProperty = ldapProperty;
        this.ldapConnectionConfig = ldapConnectionConfig;
        this.apiUserProperty = apiUserProperty;
        this.oauthPropery = oauthPropery;
        this.apiGwProperty = apiGwProperty;
    }

    public User retrievePatientInfo(String userId) {
        return retrievePatientInfo(userId, false, false);
    }

    private User retrievePatientInfo(String userId, boolean shouldRetrieveSDHStatus, boolean returnHashedPwd) {
        String methodName = "retrievePatientInfo";
        //start(methodName);

        Filter filter = Filter.createEqualityFilter(Constant.LDAP_SEARCH_ATTR_PATIENT, userId);
        User   user   = retrieveUserInfo(userId, filter, returnHashedPwd);
        
        if(shouldRetrieveSDHStatus && user.getUid() != null ) {
            // Query SDH Status from API GW
            SDHController sdhController = new SDHController(apiUserProperty,oauthPropery, apiGwProperty);
            UserStatus us = sdhController.getUserStatus(user.getUid());

            user.setSdhId    (us.getId());
            user.setSdhStatus(String.valueOf(us.getStatus()).toUpperCase());
        }
       // completed(methodName);
        return user;
    }

    public boolean isEmailExist(String email) {
        String methodName = "isEmailExist";
        //start(methodName);
        boolean result;
        Filter filter = Filter.createEqualityFilter(Constant.LDAP_SEARCH_ATTR_MAIL, email);

        try (LDAPConnection conn = ldapConnectionConfig.getServiceConnection()) {

            Optional<SearchResultEntry> entry = LDAPHelper.performSearch(conn, filter, ldapProperty.getBaseDn(), SearchRequest.ALL_USER_ATTRIBUTES);

            result = entry.isPresent();

        } catch (Exception ex) {
            log.debug(methodName, ex);
            result = false;
        }
        
        //completed(methodName);
        return result;
    } 

    private User retrieveUserInfo(String userId, Filter filter, boolean returnHashedPwd) {
        String methodName = "retrieveUserInfo";
        //start(methodName);
        User user = new User();

        try (LDAPConnection conn = ldapConnectionConfig.getServiceConnection()) {

            Optional<SearchResultEntry> entry = LDAPHelper.performSearch(conn, filter, ldapProperty.getBaseDn(), SearchRequest.ALL_USER_ATTRIBUTES);

            entry.ifPresent(obj -> {
                user.setUserId(userId);
                user.setFirstName(obj.getAttributeValue(LDAPAttribute.CN));
                user.setLastName(obj.getAttributeValue(LDAPAttribute.SN));
                user.setContactNo(obj.getAttributeValue(LDAPAttribute.MOBILE));
                user.setEmail(obj.getAttributeValue(LDAPAttribute.MAIL));
                user.setCountryCode(obj.getAttributeValue(LDAPAttribute.COUNTRY_CODE));
                user.setDob(obj.getAttributeValue(LDAPAttribute.DOB));
                user.setNric(obj.getAttributeValue(LDAPAttribute.NRIC));
                user.setEmployeeType(obj.getAttributeValue(LDAPAttribute.EMPLOYEE_TYPE));
                user.setGender(obj.getAttributeValue(LDAPAttribute.GENDER));
                user.setUid(obj.getAttributeValue(LDAPAttribute.UID));
                if (returnHashedPwd) {
                    user.setPwdHashed(obj.getAttributeValue(LDAPAttribute.USER_PASSWORD));
                }
            });

        } catch (Exception ex) {
            log.debug(methodName, ex);
        }
        //completed(methodName);
        return user;
    }

    public boolean updatePatients(User user, UserRequest request) {
        String methodName = "updatePatients";
        //start(methodName);

        boolean status = false;

        if (user != null) {
            List<Modification> mods = buildModificationsForPatients(user, request);

            if (!mods.isEmpty()) {
                status = updateUserInfo(user.getEmail(), Constant.LDAP_SEARCH_ATTR_PATIENT, mods);
            }
        }

        //completed(methodName);
        return status;
    }

    private List<Modification> buildModificationsForPatients(User user, UserRequest request) {

        List<Modification> mods = new ArrayList<>();

            Modification mod = new Modification(ModificationType.REPLACE, LDAPAttribute.EMPLOYEE_TYPE, request.getEmployeeType());
            mods.add(mod);

        return mods;
    }

    private boolean updateUserInfo(String userId, String searchFilter, List<Modification> mods) {
        String methodName = "updateUserInfo";
        //start(methodName);
        boolean result = false;

        SearchResult searchResult;
        try (LDAPConnection conn = ldapConnectionConfig.getServiceConnection()) {

            Filter filter = Filter.createEqualityFilter(searchFilter, userId);
            searchResult  = conn.search(ldapProperty.getBaseDn(), SearchScope.ONE, filter);

            if (searchResult.getEntryCount() != 0) {
                SearchResultEntry entry = searchResult.getSearchEntries().get(0);
                String userDn = entry.getDN();

                ModifyRequest modifyRequest = new ModifyRequest(userDn, mods);
                LDAPResult ldapResult = conn.modify(modifyRequest);

                result = ldapResult.getResultCode() == ResultCode.SUCCESS;
            }

        } catch (LDAPException ex) {
            log.debug(methodName, ex.getMessage());
        }

       // completed(methodName);
        return result;
    }

}
