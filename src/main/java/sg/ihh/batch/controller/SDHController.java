package sg.ihh.batch.controller;

import sg.ihh.batch.controller.base.BaseAPIController;
import sg.ihh.batch.model.UserStatus;
import sg.ihh.batch.model.UserStatusRequest;
import sg.ihh.batch.model.UserStatusResponse;
import sg.ihh.batch.util.http.HTTPClient;
import sg.ihh.batch.util.http.model.HTTPResponse;
import sg.ihh.batch.util.json.JsonUtil;
import sg.ihh.batch.util.property.ApiGwProperty;
import sg.ihh.batch.util.property.ApiUserProperty;
import sg.ihh.batch.util.property.OauthPropery;

public class SDHController extends BaseAPIController {

    private ApiUserProperty apiUserProperty;
    private OauthPropery oauthPropery;
    private ApiGwProperty apiGwProperty;

    public SDHController(ApiUserProperty apiUserProperty, OauthPropery oauthPropery, ApiGwProperty apiGwProperty) {
        super(oauthPropery,apiUserProperty, apiGwProperty);
        log = getLogger(this.getClass());
        this.apiUserProperty = apiUserProperty;
        this.oauthPropery= oauthPropery;
        this.apiGwProperty = apiGwProperty;
    }

    public UserStatus getUserStatus(String uid) {
        String methodName = "getUserStatus";
        //start(methodName);
        final String url = apiUserProperty.getStatusUrl();
        UserStatus status = null;

        // Request
        UserStatusRequest usr = new UserStatusRequest();
        usr.setUid(uid);
        filteredLogRequest(methodName, usr);

        // POST
        HTTPResponse httpResponse = HTTPClient.post(buildProtectedAPIRequest(url), JsonUtil.toJson(usr), apiUserProperty.isDoLog());
        filteredLogResponse(methodName, httpResponse.getCode());
        filteredLogResponse(methodName, httpResponse.getBody());

        // Response
        UserStatusResponse response = JsonUtil.fromJson(httpResponse.getBody(), UserStatusResponse.class);
        if (response != null && response.getResponseCode() == 200) {
            status = response.getUser();
        }

        //completed(methodName);
        return status;
    }

    // LDAP Date Format : YYYY-MM-DD (1990-01-01)
    // SDH Date Format: YYYYMMDD (19000101)
    private String convertDob(String ldapDob) {
        return ldapDob.substring(0, 4) + ldapDob.substring(5, 7) + ldapDob.substring(8, 10);
    }
}
