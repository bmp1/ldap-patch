package sg.ihh.batch.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import sg.ihh.batch.config.LDAPConnectionConfig;
import sg.ihh.batch.listener.JobListener;
import sg.ihh.batch.model.*;
import sg.ihh.batch.tasklet.*;
import sg.ihh.batch.util.property.ApiGwProperty;
import sg.ihh.batch.util.property.ApiUserProperty;
import sg.ihh.batch.util.property.LdapProperty;
import sg.ihh.batch.util.property.OauthPropery;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class LdapPatchJob {

    @Autowired
    public StepBuilderFactory stepBuilder;

    private List<String> userList = new ArrayList<>();

    @Autowired
    public LdapPatchJob(JobBuilderFactory jobBuilder, StepBuilderFactory stepBuilder) {
        this.stepBuilder = stepBuilder;
    }

    @Autowired
    private LDAPConnectionConfig ldapConnectionConfig;
    @Autowired
    protected LdapProperty ldapProperty;

    @Autowired
    private ApiUserProperty apiUserProperty;

    @Autowired
    private ApiGwProperty apiGwProperty;

    @Autowired
    private OauthPropery oauthPropery;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Bean
    Job job() {
        return this.jobBuilderFactory.get("ldapPatchJob")
                .start(readUserListStep())
                .next(ldapPatchStep())
                .build();
    }

    @Bean
    @Qualifier("readUserList")
    public Step readUserListStep() {
        return this.stepBuilder.get("sourceFileReaderStep")
                .tasklet(new ReadUserListTasklet(userList))
                .build();
    }
    @Bean
    @Qualifier("ldapPatch")
    public Step ldapPatchStep() {
        return this.stepBuilder.get("insertMedProAssocTreatmentStep")
                .tasklet(new LdapPatchTasklet(userList, ldapProperty, ldapConnectionConfig, apiUserProperty,
                        oauthPropery, apiGwProperty))
                .build();
    }

}