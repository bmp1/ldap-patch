package sg.ihh.batch.model;

import java.util.UUID;

public class SecurityHeader {
    private String state;
    
    public SecurityHeader() {
        this.state = UUID.randomUUID().toString();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    
}
