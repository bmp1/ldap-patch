package sg.ihh.batch.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserStatusRequest extends BaseAPIRequest {

    @JsonProperty("uid")
    private String uid;

    public UserStatusRequest() {
        // Empty Constructor
    }

    public String getUid() {
        return uid;
    }


    public void setUid(String uid) {
        this.uid = uid;
    }
    
    
}
