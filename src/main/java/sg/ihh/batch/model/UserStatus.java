package sg.ihh.batch.model;

public class UserStatus {

    private Integer id;
    private String  uid;
    private boolean status;
    
    public UserStatus() {
        // Empty Constructor
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    
}
