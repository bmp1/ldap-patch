package sg.ihh.batch.model.scim;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SCIMMeta {

    @JsonProperty("resourceType")
    private String resourceType;
    @JsonProperty("created")
    private String created;
    @JsonProperty("lastModified")
    private String lastModified;
    @JsonProperty("location")
    private String location;

    public SCIMMeta() {
        // Empty Constructor
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

}

