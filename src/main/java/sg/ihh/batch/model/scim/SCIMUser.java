package sg.ihh.batch.model.scim;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class SCIMUser {

    @JsonProperty("schemas")
    private List<String> schemaList;

    @JsonProperty("userName")
    private String userName;

    @JsonProperty("name")
    private SCIMName name;

    @JsonProperty("displayName")
    private String displayName;

    @JsonProperty("emails")
    private List<SCIMEmail> emailList;

    @JsonProperty("urn:ietf:params:scim:schemas:extension:gluu:2.0:User")
    private SCIMExtension extension;

    @JsonProperty("active")
    private boolean active;

    public SCIMUser() {
        schemaList = new ArrayList<>();
        this.active = true;
    }

    public List<String> getSchemaList() {
        return schemaList;
    }

    public void setSchemaList(List<String> schemaList) {
        this.schemaList = schemaList;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public SCIMName getName() {
        return name;
    }

    public void setName(SCIMName name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<SCIMEmail> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<SCIMEmail> emailList) {
        this.emailList = emailList;
    }

    public SCIMExtension getExtension() {
        return extension;
    }

    public void setExtension(SCIMExtension extension) {
        this.extension = extension;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
