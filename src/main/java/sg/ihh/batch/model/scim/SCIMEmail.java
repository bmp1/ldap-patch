package sg.ihh.batch.model.scim;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SCIMEmail {

    @JsonProperty("value")
    private String value;
    @JsonProperty("primary")
    private boolean primary;

    public SCIMEmail() {
        // Empty Constructor
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

}