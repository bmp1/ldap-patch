package sg.ihh.batch.model.scim;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SCIMSearchResponse {

    @JsonProperty("schemas")
    private List<String> schemaList;

    @JsonProperty("totalResults")
    private int totalResults;

    @JsonProperty("startIndex")
    private int startIndex;

    @JsonProperty("itemsPerPage")
    private int itemsPerPage;

    @JsonProperty("Resources")
    private List<SCIMSearchResource> resourceList;

    public SCIMSearchResponse() {
        // Empty Constructor
    }

    public List<String> getSchemaList() {
        return schemaList;
    }

    public void setSchemaList(List<String> schemaList) {
        this.schemaList = schemaList;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public List<SCIMSearchResource> getResourceList() {
        return resourceList;
    }

    public void setResourceList(List<SCIMSearchResource> resourceList) {
        this.resourceList = resourceList;
    }

}

