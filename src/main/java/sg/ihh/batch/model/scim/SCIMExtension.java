package sg.ihh.batch.model.scim;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SCIMExtension {

    @JsonProperty("pplID")
    private String nric;

    @JsonProperty("employeeType")
    private String employeeType;

    @JsonProperty("pplDOB")
    private String dob;

    @JsonProperty("pplGender")
    private String gender;

    @JsonProperty("mobile")
    private String mobileNumber;

    @JsonProperty("pplCountryCode")
    private String mobileCountryCode;

    public SCIMExtension() {
        // Empty Constructor
    }

    public String getNric() {
        return nric;
    }

    public void setNric(String nric) {
        this.nric = nric;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileCountryCode() {
        return mobileCountryCode;
    }

    public void setMobileCountryCode(String mobileCountryCode) {
        this.mobileCountryCode = mobileCountryCode;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }
}

