package sg.ihh.batch.model.scim;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SCIMSearchResource {

    @JsonProperty("schemas")
    private List<String> schemaList;

    @JsonProperty("meta")
    private SCIMMeta metaInfo;

    @JsonProperty("id")
    private String id;

    @JsonProperty("userName")
    private String userName;

    @JsonProperty("name")
    private SCIMName name;

    @JsonProperty("displayName")
    private String displayName;

    @JsonProperty("emails")
    private List<SCIMEmail> emailList;

    @JsonProperty("active")
    private boolean active;

    public SCIMSearchResource() {
        // Empty Constructor
    }

    public List<String> getSchemaList() {
        return schemaList;
    }

    public void setSchemaList(List<String> schemaList) {
        this.schemaList = schemaList;
    }

    public SCIMMeta getMetaInfo() {
        return metaInfo;
    }

    public void setMetaInfo(SCIMMeta metaInfo) {
        this.metaInfo = metaInfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public SCIMName getName() {
        return name;
    }

    public void setName(SCIMName name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<SCIMEmail> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<SCIMEmail> emailList) {
        this.emailList = emailList;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}