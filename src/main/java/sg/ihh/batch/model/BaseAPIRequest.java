package sg.ihh.batch.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseAPIRequest {

    @JsonProperty("securityHeader")
    private SecurityHeader securityHeader;

    public BaseAPIRequest() {
        securityHeader = new SecurityHeader();
    }

    public SecurityHeader getSecurityHeader() {
        return securityHeader;
    }

    public void setSecurityHeader(SecurityHeader securityHeader) {
        this.securityHeader = securityHeader;
    }
}