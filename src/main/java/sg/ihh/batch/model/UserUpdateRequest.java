package sg.ihh.batch.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserUpdateRequest extends BaseAPIRequest {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("mobileNo")
    private String mobileNo;

    @JsonProperty("identificationID")
    private String identificationID;

    @JsonProperty("email")
    private String email;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("mobileNoCountryCode")
    private String mobileNoCountryCode;

    public UserUpdateRequest() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getIdentificationID() {
        return identificationID;
    }

    public void setIdentificationID(String identificationID) {
        this.identificationID = identificationID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMobileNoCountryCode() {
        return mobileNoCountryCode;
    }

    public void setMobileNoCountryCode(String mobileNoCountryCode) {
        this.mobileNoCountryCode = mobileNoCountryCode;
    }

}

