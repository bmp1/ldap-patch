package sg.ihh.batch.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserRequest {

    @JsonProperty("uid")
    private String uid;

    @JsonProperty("userId")
    private String userId;

    @JsonProperty("email")
    private String email;

    @JsonProperty("mobile")
    private String contactNo;

    @JsonProperty("countryCode")
    private String countryCode;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("nric")
    private String nric;

    @JsonProperty("employeeType")
    private String employeeType;

    @JsonProperty("gender")
    private String gender;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNric() {
        return nric;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    public void setNric(String nric) {
        this.nric = nric;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(").append(this.email).append(",");
        sb.append(this.countryCode).append(",");
        sb.append(this.contactNo).append(",");

        if (this.nric != null) {
            sb.append(this.nric).append(",");
        }
        sb.append(this.dob).append(",");
        sb.append(this.gender).append(")");

        return sb.toString();
    }
}
