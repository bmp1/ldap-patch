package sg.ihh.batch.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


import java.io.Serializable;
import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class User implements Serializable {

    private static final long serialVersionUID = 7438401439345610863L;

    @JsonProperty("uid")
    private String uid;

    @JsonProperty("userId")
    private String userId;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("email")
    private String email;

    @JsonProperty("mobile")
    private String contactNo;

    @JsonProperty("countryCode")
    private String countryCode;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("nric")
    private String nric;

    @JsonProperty("employeeType")
    private String employeeType;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("sdhId")
    private Integer sdhId;   // sdh database table row ID for this user

    @JsonProperty("sdhStatus")
    private String sdhStatus;

    @JsonProperty("pwdHashed")
    private String pwdHashed;

    public User() {
        // Empty Constructor
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNric() {
        return nric;
    }

    public void setNric(String nric) {
        this.nric = nric;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getSdhId() {
        return sdhId;
    }

    public void setSdhId(Integer sdhId) {
        this.sdhId = sdhId;
    }

    public String getSdhStatus() {
        return sdhStatus;
    }

    public void setSdhStatus(String sdhStatus) {
        this.sdhStatus = sdhStatus;
    }

    public String getPwdHashed() {
        return pwdHashed;
    }

    public void setPwdHashed(String pwdHashed) {
        this.pwdHashed = pwdHashed;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(").append(this.email).append(",");
        sb.append(this.countryCode).append(",");
        sb.append(this.contactNo).append(",");

        if (this.nric != null) {
            sb.append(this.nric).append(",");
        }
        sb.append(this.dob).append(")");

        return sb.toString();
    }
}

