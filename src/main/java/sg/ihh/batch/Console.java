package sg.ihh.batch;

public class Console {

    public static void main (String [] args)
    {
        String[] strArr =new String [] {"site_id", "entity_id", "site_type", "name", "address_1", "address_2", "city", "state_province", "postal_code", "county_id", "telephone", "fax", "url", "email", "contact", "manager", "backline", "start_date", "start_date_text", "end_date", "end_date_text", "npi", "tax_id_number", "medicare_number", "medica_id_number", "id_number", "hours_of_operation", "next_visit", "next_visit_text", "next_visit_by", "next_visit_reason", "next_visit_notes", "next_visit_notes_html", "publish", "in_use", "comments", "comments_html", "last_updated", "last_updated_text", "monday_hours", "tuesday_hours", "wednesday_hours", "thursday_hours", "friday_hours", "saturday_hours", "sunday_hours", "billing_address", "billing_address_2", "billing_city", "billing_state", "billing_zip", "billing_county_id", "billing_country_id", "billing_telephone", "billing_fax", "billing_npi", "billing_tax_id", "billing_address_source_id", "billing_email", "billing_url", "mailing_address", "mailing_address_2", "mailing_city", "mailing_state", "mailing_county_id", "mailing_zip", "mailing_country_id", "mailing_telephone", "mailing_fax", "mailing_npi", "mailing_tax_id", "mailing_address_source_id", "mailing_email", "mailing_url", "custom_site_id", "is_credentialed", "address_block", "use_custom_fields", "custom_column_id", "object_type", "facility_type", "column_id", "billing_address_id", "mailing_address_id", "parent_field", "user_name", "object_name", "load_existing_object", "sub_type", "lazy_load", "id", "object_description", "relative_object_name", "is_new", "ignore_required_fields", "is_loaded"};
        for (String str : strArr)
        {
            System.out.println("target."+str +" = " + "raw."+str +", ");
        }
    }
}
