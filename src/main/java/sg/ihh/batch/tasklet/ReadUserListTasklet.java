package sg.ihh.batch.tasklet;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadUserListTasklet extends BaseTasklet implements Tasklet {

    private List<String> userList = new ArrayList<>();

    public ReadUserListTasklet(List<String> userList) {
        log = getLogger(this.getClass());
        this.userList = userList;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return ExitStatus.COMPLETED;
    }
    @Override
    public void beforeStep(StepExecution stepExecution) {
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
        String methodName = "executeReadList";
        //start(methodName);
        readUserList();
        //completed(methodName);
        return RepeatStatus.FINISHED;
    }

    private void readUserList() {

        String methodName = "readUserList";
        //start(methodName);
        String fileLocation = "files/source/user_list.txt";
        getFileContent(fileLocation);
        log.debug(methodName, "Found : " + userList.size() + " emails");
        //completed(methodName);
    }

    private List<String> getFileContent(String filename) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String line;

            while ((line = reader.readLine()) != null) {
                userList.add(line);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return userList;
    }

}
