package sg.ihh.batch.tasklet;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import sg.ihh.batch.config.LDAPConnectionConfig;
import sg.ihh.batch.controller.LdapController;
import sg.ihh.batch.controller.SCIMController;
import sg.ihh.batch.controller.SDHController;
import sg.ihh.batch.model.User;
import sg.ihh.batch.model.UserRequest;
import sg.ihh.batch.util.property.ApiGwProperty;
import sg.ihh.batch.util.property.ApiUserProperty;
import sg.ihh.batch.util.property.LdapProperty;
import sg.ihh.batch.util.property.OauthPropery;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class LdapPatchTasklet extends BaseTasklet implements Tasklet {

    private List<String> userList = new ArrayList<>();

    private LdapController ldapController;
    private SCIMController scimController;
    private SDHController sdhController;

    public LdapPatchTasklet(List<String> userList, LdapProperty ldapProperty,
                            LDAPConnectionConfig ldapConnectionConfig, ApiUserProperty apiUserProperty,
                            OauthPropery oauthPropery, ApiGwProperty apiGwProperty) {
        log = getLogger(this.getClass());
        this.userList = userList;
        this.ldapController = new LdapController(ldapProperty,ldapConnectionConfig, apiUserProperty, oauthPropery,
                apiGwProperty);
        this.scimController = new SCIMController(oauthPropery, apiUserProperty, apiGwProperty);
        this.sdhController = new SDHController(apiUserProperty, oauthPropery, apiGwProperty);
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
        String methodName = "execute";
        //start(methodName);
        ldapPatch();
        //completed(methodName);
        return RepeatStatus.FINISHED;
    }

    private void ldapPatch() {

        String methodName = "doPatch";
        start(methodName);

        int count = 1;
        for (String email : userList) {
            log.debug(methodName, "\n");
            log.debug(methodName, "============================      " +count + ". "+ email +"     ===================================");

            log.debug(methodName, "Validate user : " + email);
            // validate user exist
            boolean exist = ldapController.isEmailExist(email);

            if (exist) {
                //retrieve user information
                User user = ldapController.retrievePatientInfo(email);

                //validating have valid nric / passport
                boolean validNric = validateNric(user.getNric());

                //building modification request
                UserRequest userRequest = generateUserRequest(user);

                if (validNric) {
                    log.debug(methodName, "Found valid NRIC (" + user.getNric() + "), employeeType  set to nric");
                    userRequest.setEmployeeType("nric");
                } else {
                    log.debug(methodName, "No valid NRIC found (" + user.getNric() + "), employeeType set to passport");
                    userRequest.setEmployeeType("passport");

                }

                // get user information
                User patient = ldapController.retrievePatientInfo(user.getEmail());

                boolean status = false;// ldapController.updatePatients(patient, userRequest);
                log.debug(methodName, "LDAP UPDATE STATUS :" + status);
                if (status) {
                    boolean scimStatus = scimController.update(patient, userRequest);
                    log.debug(methodName, "SCIM UPDATE STATUS :" + scimStatus);
                }
            } else {
                log.debug(methodName, "user with email " + email + " not found!");
            }
            count ++;
        }
        completed(methodName);
    }


    public boolean validateNric(String inputString) {
        String nricToTest = inputString.toUpperCase();

        // first letter must start with S, T, F, G or M. Last letter must be A - Z
        if (!Pattern.compile("^[STFGM]\\d{7}[A-Z]$").matcher(nricToTest).matches()) {
            return false;
        } else {
            char[] icArray = new char[9];
            char[] st = "JZIHGFEDCBA".toCharArray();
            char[] fg = "XWUTRQPNMLK".toCharArray();
            char[] m = "KLJNPQRTUWX".toCharArray();

            for (int i = 0; i < 9; i++) {
                icArray[i] = nricToTest.charAt(i);
            }

            // calculate weight of positions 1 to 7
            int weight = (Integer.parseInt(String.valueOf(icArray[1]), 10)) * 2 +
                    (Integer.parseInt(String.valueOf(icArray[2]), 10)) * 7 +
                    (Integer.parseInt(String.valueOf(icArray[3]), 10)) * 6 +
                    (Integer.parseInt(String.valueOf(icArray[4]), 10)) * 5 +
                    (Integer.parseInt(String.valueOf(icArray[5]), 10)) * 4 +
                    (Integer.parseInt(String.valueOf(icArray[6]), 10)) * 3 +
                    (Integer.parseInt(String.valueOf(icArray[7]), 10)) * 2;

            int offset = icArray[0] == 'T' || icArray[0] == 'G' ? 4 : icArray[0] == 'M' ? 3 : 0;

            int lastCharPosition = (offset + weight) % 11;

            if (icArray[0] == 'M') {
                lastCharPosition = 10 - lastCharPosition;
            }

            if (icArray[0] == 'S' || icArray[0] == 'T') {
                return icArray[8] == st[lastCharPosition];
            } else if (icArray[0] == 'F' || icArray[0] == 'G') {
                return icArray[8] == fg[lastCharPosition];
            } else if (icArray[0] == 'M') {
                return icArray[8] == m[lastCharPosition];
            } else {
                return false; // this line should never reached due to regex above
            }
        }
    }

    public UserRequest generateUserRequest(User user) {
        UserRequest userRequest = new UserRequest();
        userRequest.setUid(user.getUid());
        userRequest.setUserId(user.getUserId());
        userRequest.setEmail(user.getEmail());
        userRequest.setContactNo(user.getContactNo());
        userRequest.setCountryCode(user.getCountryCode());
        userRequest.setDob(user.getDob());
        userRequest.setNric(user.getNric());
        userRequest.setEmployeeType(user.getEmployeeType());
        userRequest.setGender(user.getGender());

        return userRequest;
    }
}
