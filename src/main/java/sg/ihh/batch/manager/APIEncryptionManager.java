package sg.ihh.batch.manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import parkway.sdh.gw.encryption.controller.APIUtil;
import parkway.sdh.gw.encryption.model.Payload;
import sg.ihh.batch.util.property.EncryptionProperty;
import sg.ihh.batch.util.property.LdapProperty;

public class APIEncryptionManager extends BaseManager {

   @Autowired
   private EncryptionProperty encryptionProperty;

    private static APIEncryptionManager instance;

    private APIUtil apiUtil;

    public APIEncryptionManager() {
        log = getLogger(LdapProperty.class);
        log.info("APIEncryptionManager Initiated");
        String decryptPrivateKey = EncryptionManager.getInstance()
                .decrypt(encryptionProperty.getAppServerKey());
        String publicKey = encryptionProperty.getApiGwPublicKey();
        apiUtil = new APIUtil(publicKey, decryptPrivateKey);
    }


    public Payload encrypt(String message) {
        return apiUtil.encrypt(message);
    }

    public String decrypt(Payload payload) {
        return apiUtil.decrypt(payload);
    }

    public static APIEncryptionManager getInstance() {

        if (instance == null) {
            instance = new APIEncryptionManager();
        }

        return instance;
    }

}

