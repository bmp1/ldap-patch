package parkway.sdh.gw.encryption.controller;

import org.apache.logging.log4j.Logger;

import javax.crypto.Cipher;

public class BaseController {

    protected Cipher encryptCipher;
    protected Cipher decryptCipher;
    protected String cipherMode;
    protected Logger log;

    public BaseController() {
        // Empty Constructor
    }

    protected void error(String methodName, Exception ex) {
        log.error(() -> methodName, ex);
    }
}
